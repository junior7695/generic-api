<?php

namespace App\Events;

use Infrastructure\Events\Event;
use App\Role;

class RoleWasUpdated
{
    public $role;

    public function __construct(Role $role)
    {
        $this->role = $role;
    }
}
