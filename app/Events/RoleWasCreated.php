<?php

namespace App\Events;

use Infrastructure\Events\Event;
use App\Role;

class RoleWasCreated
{
    public $role;

    public function __construct(Role $role)
    {
        $this->role = $role;
    }
}
