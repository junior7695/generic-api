<?php

namespace App;

use App\Providers\EventServiceProvider;
use App\Events\RoleWasCreated;
use App\Events\RoleWasDeleted;
use App\Events\RoleWasUpdated;
use App\Events\UserWasCreated;
use App\Events\UserWasDeleted;
use App\Events\UserWasUpdated;

class UserServiceProvider extends EventServiceProvider
{
    protected $listen = [
        RoleWasCreated::class => [
            // listeners for when a role is created
        ],
        RoleWasDeleted::class => [
            // listeners for when a role is deleted
        ],
        RoleWasUpdated::class => [
            // listeners for when a role is updated
        ],
        UserWasCreated::class => [
            // listeners for when a user is created
        ],
        UserWasDeleted::class => [
            // listeners for when a user is deleted
        ],
        UserWasUpdated::class => [
            // listeners for when a user is updated
        ]
    ];
}
