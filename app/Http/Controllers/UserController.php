<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Requests\CreateUserRequest;
use App\Requests\UserRolesRequest;
use App\Services\UserService;
use Optimus\Bruno\EloquentBuilderTrait;
use Optimus\Bruno\LaravelController;
use App\Http\Controllers\Controller;

class UserController extends Controller
{
    use EloquentBuilderTrait;
    private $userService;

    public function __construct(UserService $userService)
    {
        $this->userService = $userService;
    }

    public function getAll()
    {
        $resourceOptions = $this->parseResourceOptions();

        //$query = User::query();
        //$this->applyResourceOptions($query, $resourceOptions);
        //$data = $query->get();
        $data = $this->userService->getAll($resourceOptions);
        $parsedData = $this->parseData($data, $resourceOptions, 'users');


        return $this->response($parsedData);
        //return responder()->success($parsedData)->respond();
    }

    public function getById($userId)
    {
        $resourceOptions = $this->parseResourceOptions();

        $data = $this->userService->getById($userId, $resourceOptions);
        $parsedData = $this->parseData($data, $resourceOptions, 'user');

        return $this->response($parsedData);
        //return responder()->success($parsedData)->respond();
    }

    public function create(CreateUserRequest $request)
    {
        $data = $request->get('user', []);

        return $this->response($this->userService->create($data), 201);
        //return responder()->success($data)->respond();
    }

    public function update($userId, Request $request)
    {
        $data = $request->get('user', []);

        return $this->response($this->userService->update($userId, $data));
    }

    public function delete($userId)
    {
        return $this->response($this->userService->delete($userId));
    }

    public function addRoles($userId, UserRolesRequest $request)
    {
        $roles = $request->get('roles', []);

        return $this->response($this->userService->addRoles($userId, $roles));
    }

    public function setRoles($userId, UserRolesRequest $request)
    {
        $roles = $request->get('roles', []);

        return $this->response($this->userService->setRoles($userId, $roles));
    }

    public function removeRoles($userId, UserRolesRequest $request)
    {
        $roles = $request->get('roles', []);

        return $this->response($this->userService->removeRoles($userId, $roles));
    }
}
