<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ResponseAPI extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function responseCreate(Response $response){

    }
}





/*
Para esta actividad, se debe diseñar un proyecto donde se definan los métodos para los require y los responses. Es importante establecer un patrón estándar para todos los ámbitos del negocio. Validar según la petición (ya sea GET, PUT, POST, DELETE) una respuesta bajo la siguiente estructura:

Para URL:

·         Elemento: /ejemplo/

·         Acción sobre un elemento: ejemplo/ID_elemento/

Para HTTP:

INSERT

·         403 (Acceso prohibido)

·         400 (petición incorrecta)

·         500 (Error del lado del servidor al intentar crear el canal en BD)

·         201 (Creado correctamente)

UPDATE

·         403 (Acceso prohibido)

·         400 (petición incorrecta)

·         500 (Error del lado del servidor)

·         200 (Modificado correctamente)

DELETE

·         200 OK

·         404 Not found

·         500 Server error

CONSULTAS

·         200 OK

·         404 Not found

·         500 Server error

La estructura estandarizada de respuesta debe manejarse de una forma clara, fácil de comprender.

Tener en cuenta que las respuestas obtenidas del api deben ser un JSON con la estructura:

 \{

     "attribute": “value” ,

     "object": 

     \{

           "Attribute 1": “value”,

           "Attribute 2": “value”

     \}

     "list": 

\[

           "item 1",

           "item 2"

\],

       "list_objet":

\[

           \{

           "Attribute 1": “value”,

           "Attribute 2": “value”

           \},

           \{

           "Attribute 1": “value”,

           "Attribute 2": “value”

          \}

\]

\}
*/