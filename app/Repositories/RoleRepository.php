<?php

namespace App\Repositories;

use App\Role;
use Illuminate\Database\Query\Builder;
use Optimus\Genie\Repository as BaseRepository;

class RoleRepository extends BaseRepository
{
    public function getModel()
    {
        return new Role();
    }

    public function create(array $data)
    {
        $role = $this->getModel();

        $role->fill($data);
        $role->save();

        return $role;
    }

    public function update(Role $role, array $data)
    {
        $role->fill($data);

        $role->save();

        return $role;
    }

    public function filterIsAgent(Builder $query, $method, $clauseOperator, $value, $in)
    {
        // check if value is true
        if ($value) {
            $query->whereIn('roles.name', ['Agent']);
        }
    }
}
