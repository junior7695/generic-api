<?php

namespace App\Requests;

use App\Requests\ApiRequest;

class UserRolesRequest extends ApiRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'roles' => 'array|required'
        ];
    }
}
