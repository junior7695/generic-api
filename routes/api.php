<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/*Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});*/
Route::get('/user', 'UserController2@index');

Route::get('/user/{id}', 'UserController2@show');

Route::get('/users', 'UserController@getAll');
Route::get('/users/{id}', 'UserController@getById');
Route::post('/users', 'UserController@create');
Route::put('/users/{id}', 'UserController@update');
Route::delete('/users/{id}', 'UserController@delete');
Route::post('/users/{id}/roles', 'UserController@addRoles');
Route::put('/users/{id}/roles', 'UserController@setRoles');
Route::delete('/users/{id}/roles', 'UserController@removeRoles');

Route::get('/roles', 'RoleController@getAll');
Route::get('/roles/{id}', 'RoleController@getById');
Route::post('/roles', 'RoleController@create');
Route::delete('/roles/{id}', 'RoleController@delete');